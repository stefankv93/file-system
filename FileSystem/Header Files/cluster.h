#ifndef _CLUSTER_H_
#define _CLUSTER_H_

class Partition;
struct Entry;

class Cluster
{
public:
	Cluster(Partition *p, unsigned long cn);
	Cluster(const Cluster&);
	~Cluster();

	void setClusterNo(unsigned long cn);
	unsigned long getClusterNo();
	void writeBytes(char *buffer, int count, int start = 0);
	void writeEntry(Entry &e, int pos);
	void readBytes(char *buffer, int count, int start = 0);	
	void readEntry(Entry &e, int pos);

private:
	char *cluster;
	unsigned long clusterNumber;//, currClusterNumber;
	bool v, d;
	Partition *p;

	bool check();
	void save();
	void load();
};

#endif