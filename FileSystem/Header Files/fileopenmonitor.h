#ifndef _FILE_OPEN_MONITOR_H_
#define _FILE_OPEN_MONITOR_H_

#include <Windows.h>
#include <deque>

using namespace std;

class FileOpenMonitor
{
public:
	FileOpenMonitor();
	~FileOpenMonitor();

	void openFile(CRITICAL_SECTION &critical);
	void closeFile(CRITICAL_SECTION &critical);

private:
	CONDITION_VARIABLE blocked;
	bool opened;
	deque<DWORD> *blockedIds;
};

#endif