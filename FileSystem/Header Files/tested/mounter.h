#ifndef _MOUNTER_H_
#define _MOUNTER_H_

#include "thread.h"
#include <Windows.h>


class Partition;
const int PARTITION_NUM = 26;

template <typename T>
class BoundedBuffer;

struct MounterRequest
{
	Partition* partition;
	bool mount;
	HANDLE semaphore;
	char letter;
};

class Mounter : public Thread
{
public:
	Mounter(BoundedBuffer<MounterRequest> *bb);
	virtual ~Mounter();

	virtual void run();

	Partition* getPartition(char part);

	void stop();

private:
	bool end;
	BoundedBuffer<MounterRequest> *myBuffer;
	Partition *partitions[PARTITION_NUM];
	CRITICAL_SECTION Mutex;
};


#endif