#ifndef _THREAD_H_
#define _THREAD_H_
#include <Windows.h>

class Thread
{
public:
	Thread();
	void start();
	void join();
	virtual ~Thread();

protected:
	virtual void run() = 0;
	static unsigned int __stdcall ThreadEntry(void *arg);

private:
	HANDLE handler;
	unsigned threadId;
	Thread(const Thread&);
	Thread& operator=(const Thread&);
};

#endif