#ifndef _BOUNDED_H_
#define _BOUNDED_H_

#include <Windows.h>

template <typename T>
class BoundedBuffer {
public:

	BoundedBuffer(unsigned size);
	virtual ~BoundedBuffer();
	int append(T& item);
	T* take();
	int  fullCount(){ return numInBuffer; }

private:
	friend class Consumer;
	unsigned Capacity, numInBuffer;
	CONDITION_VARIABLE spaceAvaliable, itemAvaliable;
	CRITICAL_SECTION Mutex;
	T** buffer;
	int head, tail;

};

template <typename T>
BoundedBuffer<T>::BoundedBuffer(unsigned size) :Capacity(size), numInBuffer(0), head(0), tail(0)
{
	InitializeCriticalSection(&Mutex);
	InitializeConditionVariable(&spaceAvaliable);
	InitializeConditionVariable(&itemAvaliable);
	buffer = new T*[size];
	for (int i = 0; i < size; i++)
		buffer[i] = nullptr;
}

template <typename T>
BoundedBuffer<T>::~BoundedBuffer()
{
	/*for (int i = 0; i < Capacity; i++)
		if (buffer[i] != nullptr)
			delete buffer[i];*/

	delete buffer;
	DeleteCriticalSection(&Mutex);
}

template <typename T>
int BoundedBuffer<T>::append(T& item)
{
	EnterCriticalSection(&Mutex);
	//cout << "Append!" << endl;
	while (numInBuffer == Capacity)
		SleepConditionVariableCS(&spaceAvaliable, &Mutex, INFINITE);

	buffer[tail] = &item;
	tail = (tail + 1) % Capacity;
	numInBuffer++;

	WakeConditionVariable(&itemAvaliable);
	LeaveCriticalSection(&Mutex);
	return 0;
}

template <typename T>
T* BoundedBuffer<T>::take()
{
	EnterCriticalSection(&Mutex);
	//cout << "TAKE!" << endl;
	while (numInBuffer == 0)
		SleepConditionVariableCS(&itemAvaliable, &Mutex, INFINITE);

	T* item = buffer[head];
	buffer[head] = nullptr;
	head = (head + 1) % Capacity;
	numInBuffer--;

	WakeConditionVariable(&spaceAvaliable);
	LeaveCriticalSection(&Mutex);
	return item;
}

#endif