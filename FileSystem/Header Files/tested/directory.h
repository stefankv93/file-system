#ifndef _DIRECTORY_H_
#define _DIRECTORY_H_

typedef unsigned long ClusterNo;
class Fat;

class Directory
{
	Directory(ClusterNo fc, Fat *fat);

	int createDir();

private:
	ClusterNo firstCluster;
	Fat *mFat;
};

#endif