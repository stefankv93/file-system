#ifndef _MUTEX_H_
#define _MUTEX_H_

#include <Windows.h>

class Mutex
{
public:
	Mutex(CRITICAL_SECTION& m);
	Mutex(HANDLE semHandle);
	~Mutex();

private:
	CRITICAL_SECTION &mutex;
};

#endif