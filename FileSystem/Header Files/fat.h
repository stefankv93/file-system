#ifndef _FAT_H_
#define _FAT_H_

#include <vector>
#include <Windows.h>
typedef unsigned long EntryNum;
typedef unsigned long ClusterNo;
const int FAT_ENTRIES = 512;
const int NUM_OF_VALUABLE_INFO = 4;
const int FREE_CLUSTERS_ENTRY = 0;
const int FAT_SIZE_ENTRY = 1;
const int ROOT_DIR_ENTRY = 2;
const int ROOT_DIR_SIZE_ENTRY = 3;

class Partition;
class Volume;
using namespace std;

class Fat
{
public:
	Fat(Partition *);
	~Fat();

	ClusterNo allocateCluster();
	int deallocateCluster(ClusterNo);

	ClusterNo getFatValue(ClusterNo);
	ClusterNo getRootEntry();
	int getRootSize();
	ClusterNo getFreeEntry();

	void setFatValue(ClusterNo, ClusterNo);
	void setFreeEntry(ClusterNo);
	void setRootDirSize(int size);
	int fatSize();
	void saveFat();

private:
	friend class Volume;
	struct FatCluster
	{
		EntryNum entries[FAT_ENTRIES];
	};

	FatCluster** clusters;
	Partition* mPartition;
	int clusterCount;
	CRITICAL_SECTION critical;
};

#endif