#ifndef _KERNELFILE_H_
#define _KERNELFILE_H_

typedef unsigned long BytesCnt;
const unsigned MAX_CLUSTER_BYTES = 2048;

class Volume;
class Cluster;
struct Entry;
#include <string>
using namespace std;

class KernelFile
{
public:
	KernelFile(char *fname, char mode, Entry *e, Volume *v);
	~KernelFile(); //file closing

	char write(BytesCnt, char* buffer);	//write to file
	BytesCnt read(BytesCnt, char* buffer);	//read from file
	char seek(BytesCnt);	//position of seeker in file
	BytesCnt filePos();	//seeker position in file
	char eof();	//end of file
	BytesCnt getFileSize(); //file size
	char truncate(); //** opciono
private:
	char mode;
	unsigned long p, q;
	Cluster *data;
	string fname;
	BytesCnt firstCluster;
	BytesCnt size;
	BytesCnt pos;
	Volume *mVolume;
};

#endif