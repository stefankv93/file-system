#ifndef _KERNELFS_H_
#define _KERNELFS_H_

#include <Windows.h>

class Partition;
class File;
struct Entry;
class Volume;

typedef unsigned long BytesCnt;
typedef unsigned long EntryNum;
const int PARTITIONS_NUM = 26;



class KernelFS
{
public:
	KernelFS();
	~KernelFS();

	char mount(Partition* partition); // mounting partition
	// returns assigned char
	// or 0 if there is no success

	char unmount(char part); // unmount partition labeled with given char
	// returns 0 if there is no success or 1 if there is success

	char format(char part); // formating partition labaled with given char
	// returns 0 if there is no success or 1 if there is success

	char doesExist(char* fname); // argument is file or directory
	// specified by absolute path

	File* open(char* fname, char mode); // first argument is file name
	// specified by absolute path and second argument is mode

	char deleteFile(char* fname); // argument is file name
	// specified by absolute path

	char createDir(char* dirname); //argument is directory name
	// specified by absolute path

	char deleteDir(char* dirname); //argument is directory name
	// specified by absolute path

	char readDir(char* dirname, EntryNum n, Entry &e);
	// first argument is specified by absolute paht, second argument
	// is specified by number of the entry that is read and third 
	// argument is the address at which the stored read entry
private:
	Volume *volumes[26];
	CRITICAL_SECTION mountCritical;

	int convertCharToInt(char c);
	

};

#endif