#ifndef _VOLUME_H_
#define _VOLUME_H_

class Partition;
class Fat;
class FileOpenTable;
class KernelFile;
class Cluster;
#include <string>
#include <vector>
#include <Windows.h>
using namespace std;

typedef unsigned long ClusterNo;
typedef unsigned long EntryNum;

const unsigned int MAX_DIR_CLUSTER_ENTRY = 102;
const unsigned int DIR_ATRIBUTE = 2;
const unsigned int FILE_ATRIBUTE = 1;

const unsigned int FNAMELEN = 8;
const unsigned int FEXTLEN = 3;

struct Entry {
	char name[FNAMELEN];
	char ext[FEXTLEN];
	char attributes;
	unsigned long firstCluster;
	unsigned long size;
};

class Volume
{
public:
	Volume(Partition *partiton, char letter);
	~Volume();
	Partition* getPartition();
	Fat* getFat();
	int format();
	void unmount();
	int doesExists(char *fname);
	KernelFile* open(char* fname, char mode);
	int close(string fname, EntryNum firstCluster, EntryNum size);
	int deleteFile(char *fname);
	int createDir(char *fname);
	int deleteDir(char *fname);
	char readDir(char* dirname, EntryNum n, Entry &e);
	int find(Cluster *dirCluster, int dirSize, ClusterNo currDirCluster, vector<string>::iterator start, vector<string>::iterator end);
	int createFileSystemObject(char *fname, int atribute);
	int deleteFileSystemObject(char *fname, int atribute);
	
private:
	//friend class KernelFile;
	Partition *mPartition;
	Fat *mFat;
	FileOpenTable *mFileTable;
	char mLetter;

	CRITICAL_SECTION critical;
	CONDITION_VARIABLE formatBlocked;
	CONDITION_VARIABLE unmountBlocked;
	bool formatAnounce, unmountAnounce;

	int makeNameAndExt(string &fname, string &makeFname, string &makeExt);
	vector<string> split(const char *str, char c = ' ');

};

#endif