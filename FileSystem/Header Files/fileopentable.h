#ifndef _FILE_OPEN_TABLE_H_
#define _FILE_OPEN_TABLE_H_

#include <unordered_set>
#include <unordered_map>
#include <Windows.h>
#include <string>
using namespace std;

class FileOpenMonitor;

struct TableEntry
{
	string fname;
	string flocation;
	FileOpenMonitor *fm;
	unordered_set<DWORD> *threads;
	unsigned declared;

	TableEntry(string fn, string fl);

	~TableEntry();

private:
	TableEntry(const TableEntry&){}
	TableEntry& operator=(const TableEntry&) {}
};

class FileOpenTable
{
public:
	FileOpenTable();
	~FileOpenTable();

	int openFile(string fullfname, CRITICAL_SECTION& critical);
	int closeFile(string fullfname, CRITICAL_SECTION& critical);
	int numOfOpenFiles();
	int isFileOpenByThread(string fullfname, DWORD id);
	int isOpen(string fullfname);
	//int isOpenByThread(string fullfname, DWORD threadId);
private:
	FileOpenTable(const FileOpenTable&) {}
	FileOpenTable& operator=(const FileOpenTable&) {}
	unordered_map<string, TableEntry*> *table;
};

#endif