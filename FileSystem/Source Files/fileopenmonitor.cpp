#include "fileopenmonitor.h"
#include "mutex.h"


FileOpenMonitor::FileOpenMonitor()
{
	InitializeConditionVariable(&blocked);
	opened = false;
	blockedIds = new deque<DWORD>();
}

FileOpenMonitor::~FileOpenMonitor()
{
	blockedIds->clear();
	delete blockedIds;
}


void FileOpenMonitor::openFile(CRITICAL_SECTION &critical)
{

	while (opened == true || !blockedIds->empty())
	{
		blockedIds->push_back(GetCurrentThreadId());
		while (opened == true || blockedIds->front() != GetCurrentThreadId())
		SleepConditionVariableCS(&blocked, &critical, INFINITE);

		blockedIds->pop_front();
		break;
	}

	opened = true;
	
}

void FileOpenMonitor::closeFile(CRITICAL_SECTION &critical)
{
	opened = false;
	WakeAllConditionVariable(&blocked);
}