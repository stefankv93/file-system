#include "kernelfs.h"
//#include "bounded.h"
//#include "mounter.h"
#include "part.h"
#include "volume.h"
#include "fileopentable.h"
#include "file.h"
#include "kernelfile.h"
#include "mutex.h"
#include <vector>
#include <string>

#include <iostream>
using namespace std;

KernelFS::KernelFS()
{
	InitializeCriticalSection(&mountCritical);
	for (int i = 0; i < PARTITIONS_NUM; i++)
		volumes[i] = 0;
}

KernelFS::~KernelFS()
{
	DeleteCriticalSection(&mountCritical);

	for (int i = 0; i < PARTITIONS_NUM; i++)
		delete volumes[i];
}

char KernelFS::mount(Partition* partition)
{
	Mutex mutex(mountCritical);

	//check if it is allready mounted
	for (int i = 0; i < PARTITIONS_NUM; i++)
		if (volumes[i] != nullptr && volumes[i]->getPartition() == partition)
		{
			return 'A' + i;
		}

	//mount partition
	int freeCount = 0;
	char letter = 0;
	for (; freeCount < PARTITIONS_NUM; freeCount++)
		if (volumes[freeCount] == nullptr)
			break;

	if (freeCount != PARTITIONS_NUM)
	{
		letter = 'A' + freeCount;
		volumes[freeCount] = new Volume(partition, letter);
	}

	return letter;
}

char KernelFS::unmount(char part)
{
	int partitionIndex = part - 'A';
	if (partitionIndex >= 0 && partitionIndex < 26)
	{
		if (volumes[partitionIndex] != nullptr) {
			volumes[partitionIndex]->unmount();
			delete volumes[partitionIndex];
			volumes[partitionIndex] = nullptr;
			return part;
		}
		else
		{
			return 0;
		}
	}
	else return 0;
}

char KernelFS::format(char part)
{
	int index = convertCharToInt(part);
	if (index == -1) return -1;

	Volume *v = volumes[index];
	if (v == 0) return -1;
	return v->format();
}


char KernelFS::doesExist(char* fname)
{
	char partitionChar = fname[0];
	int volumeIndex = convertCharToInt(partitionChar);
	if (volumeIndex == -1) return 0;

	Volume *v = volumes[volumeIndex];
	if (v == 0) return 0;
	return v->doesExists(fname);
}

File* KernelFS::open(char* fname, char mode)
{
	char partitionChar = fname[0];
	int volumeIndex = convertCharToInt(partitionChar);
	if (volumeIndex == -1) return 0;

	Volume *v = volumes[volumeIndex];
	if (v == 0) return nullptr;
	KernelFile* kf = v->open(fname, mode);
	if (kf == 0) return nullptr;
	
	File *file = new File();
	file->myImpl = kf;
	return file;
}

char KernelFS::deleteFile(char* fname)
{
	char partitionChar = fname[0];
	int volumeIndex = convertCharToInt(partitionChar);
	if (volumeIndex == -1) return 0;

	Volume *v = volumes[volumeIndex];
	if (v == 0) return 0;
	return v->deleteFile(fname);
}

char KernelFS::createDir(char* dirname)
{
	char partitionChar = dirname[0];
	int volumeIndex = convertCharToInt(partitionChar);
	if (volumeIndex == -1) return 0;

	Volume *v = volumes[volumeIndex];
	if (v == 0) return 0;
	return v->createDir(dirname);
}

char KernelFS::deleteDir(char *dirname)
{

	char partitionChar = dirname[0];
	int volumeIndex = convertCharToInt(partitionChar);
	if (volumeIndex == -1) return 0;

	Volume *v = volumes[volumeIndex];
	if (v == 0) return 0;
	return v->deleteDir(dirname);
}

char KernelFS::readDir(char* dirname, EntryNum n, Entry &e)
{
	char partitionChar = dirname[0];
	int volumeIndex = convertCharToInt(partitionChar);
	if (volumeIndex == -1) return 0;

	Volume *v = volumes[volumeIndex];
	if (v == 0) return 0;
	return v->readDir(dirname, n, e);
}

int KernelFS::convertCharToInt(char c)
{
	int ret = c - 'A';
	if (ret >= 0 && ret < 26) return ret;
	else return -1;
}