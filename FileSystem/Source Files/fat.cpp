#include "fat.h"
#include "part.h"
#include <vector>
#include "mutex.h"
using namespace std;

Fat::Fat(Partition *partition)
{
	InitializeCriticalSection(&critical);
	mPartition = partition;
	clusterCount = (fatSize() + NUM_OF_VALUABLE_INFO) / FAT_ENTRIES;
	if ((fatSize() + NUM_OF_VALUABLE_INFO) % FAT_ENTRIES) clusterCount++;
	clusters = new FatCluster*[clusterCount];
	for (int i = 0; i < clusterCount; i++)
	{
		FatCluster *fc = new FatCluster();
		mPartition->readCluster(i, (char *)fc->entries);
		clusters[i] = fc;
	}

}

Fat::~Fat()
{
	saveFat();
	for (int i = 0; i < clusterCount; i++)
		delete clusters[i];

	delete clusters;
	DeleteCriticalSection(&critical);
}

ClusterNo Fat::allocateCluster()
{
	Mutex mutex(critical);
	if (clusters == 0) return -1; //There is no Fat Table

	ClusterNo head = getFreeEntry();
	if (head == 0) return -2;	//There is no free sapce on partition

	ClusterNo returnCluster = head;
	head = getFatValue(head);
	setFreeEntry(head);
	setFatValue(returnCluster, 0);
	//saveFat();

	return returnCluster;
}

int Fat::deallocateCluster(ClusterNo cluster)
{
	Mutex mutex(critical);
	if (getFatValue(cluster) == 0)
	{
		setFatValue(cluster, getFreeEntry());
		setFreeEntry(cluster);
		return 1;
	}

	return 0;
}

ClusterNo Fat::getFatValue(ClusterNo cluster)
{
	Mutex mutex(critical);
	
	int clusterIndex = (cluster + NUM_OF_VALUABLE_INFO) / FAT_ENTRIES;
	int index = (cluster + NUM_OF_VALUABLE_INFO) % FAT_ENTRIES;
	return clusters[clusterIndex]->entries[index];
}

ClusterNo Fat::getRootEntry()
{
	Mutex mutex(critical);
	return clusters[0]->entries[ROOT_DIR_ENTRY];
}

int Fat::getRootSize()
{
	Mutex mutex(critical);
	return clusters[0]->entries[ROOT_DIR_SIZE_ENTRY];
}

ClusterNo Fat::getFreeEntry()
{
	Mutex mutex(critical);
	return clusters[0]->entries[FREE_CLUSTERS_ENTRY];
}

void Fat::setFatValue(ClusterNo entry, ClusterNo value)
{
	Mutex mutex(critical);
	int clusterIndex = (entry + NUM_OF_VALUABLE_INFO) / FAT_ENTRIES;
	int index = (entry + NUM_OF_VALUABLE_INFO) % FAT_ENTRIES;
	clusters[clusterIndex]->entries[index] = value;
}

void Fat::setRootDirSize(int size)
{
	Mutex mutex(critical);
	clusters[0]->entries[ROOT_DIR_SIZE_ENTRY] = size;
}

void Fat::setFreeEntry(ClusterNo entry)
{
	Mutex mutex(critical);
	clusters[0]->entries[FREE_CLUSTERS_ENTRY] = entry;
}

void Fat::saveFat()
{
	Mutex mutex(critical);
	for (int i = 0; i < clusterCount; i++)
	{
		FatCluster* fatCluster = clusters[i];
		mPartition->writeCluster(i, (char*)fatCluster->entries);
	}
}

int Fat::fatSize()
{
	Mutex mutex(critical);
	return mPartition->getNumOfClusters();
}