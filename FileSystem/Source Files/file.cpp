#include "file.h"
#include "kernelfile.h"

File::File()
{

}

File::~File()
{
	delete myImpl;
}

char File::write(BytesCnt bc, char* buffer)
{
	if (myImpl)
	{
		return myImpl->write(bc, buffer);
	}
	else return 0;
}

BytesCnt File::read(BytesCnt bc, char* buffer)
{
	if (myImpl)
	{
		return myImpl->read(bc, buffer);
	}
	else return 0;
}

char File::seek(BytesCnt bc)
{
	if (myImpl)
	{
		return myImpl->seek(bc);
	}
	else return 0;
}

BytesCnt File::filePos()
{
	if (myImpl)
	{
		return myImpl->filePos();
	}
	else return -1;
}

char File::eof()
{
	if (myImpl)
	{
		return myImpl->eof();
	}
	else return 1; //error
}

BytesCnt File::getFileSize()
{
	if (myImpl)
	{
		return myImpl->getFileSize();
	}
	else return -1;
}

char File::truncate()
{
	if (myImpl)
	{
		return myImpl->truncate();
	}
	else return 0;
}
	