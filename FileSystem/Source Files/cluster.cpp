#include "cluster.h"
#include "part.h"
#include "volume.h"

Cluster::Cluster(Partition *p, unsigned long cn)
{
	this->p = p;
	clusterNumber = cn;
	cluster = 0;
	v = false;
	d = false;
}

Cluster::Cluster(const Cluster& c)
{
	p = c.p;
	clusterNumber = c.clusterNumber;
	cluster = new char[2048];
	memcpy(cluster, c.cluster, 2048);
}

Cluster::~Cluster()
{
	if (d == true && v == true)
	{
		save();
	}

	if (cluster)
	{
		delete cluster;
		cluster = 0;
	}
}

void Cluster::save()
{
		p->writeCluster(clusterNumber, cluster);
}
void Cluster::load()
{
	p->readCluster(clusterNumber, cluster);

}

void Cluster::writeBytes(char *buffer, int count, int start)
{
	if (count + start > 2048) return;

	if (v == false)
	{
		if (cluster == 0) cluster = new char[2048];

		load();
		v = true;
	}

	memcpy(cluster + start, buffer, count);
	d = true;
}

void Cluster::writeEntry(Entry &e, int pos)
{
	if (pos > 102) return;

	if (v == false)
	{
		if (cluster == 0) cluster = new char[2048];

		load();
		v = true;
	}

	memcpy(((Entry *)cluster + pos), &e, sizeof(Entry));
	d = true;
}

void Cluster::setClusterNo(unsigned long cn)
{
	if (cn != clusterNumber)
	{
		v = false;

		if (d == true)
			save();

		d = false;
		clusterNumber = cn;
	}
}

unsigned long Cluster::getClusterNo()
{
	return clusterNumber;
}

void Cluster::readBytes(char *buffer, int count, int start)
{
	if (v == false)
	{
		if (cluster == 0) cluster = new char[2048];


		load();
		v = true;
	}

	memcpy(buffer, cluster + start, count);
}

void Cluster::readEntry(Entry &e, int pos)
{
	if (v == false)
	{
		if (cluster == 0) cluster = new char[2048];

		load();
		v = true;
	}

	memcpy(&e, ((Entry*)cluster + pos), sizeof(Entry));
}

bool Cluster::check()
{
	return v == true;
}