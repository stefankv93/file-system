#include "volume.h"
#include "part.h"
#include "fat.h"
#include "cluster.h"
#include "fileopentable.h"
#include "kernelfile.h"
#include "mutex.h"

Volume::Volume(Partition *partiton, char letter)
{
	InitializeCriticalSection(&critical);
	InitializeConditionVariable(&formatBlocked);
	InitializeConditionVariable(&unmountBlocked);
	mPartition = partiton;
	mFat = new Fat(mPartition);
	mLetter = letter;
	mFileTable = new FileOpenTable();
	formatAnounce = false;
	unmountAnounce = false;
}

Volume::~Volume()
{
	mPartition = 0;
	delete mFat;
	delete mFileTable;
	DeleteCriticalSection(&critical);
}

Partition* Volume::getPartition()
{
	return mPartition;
}

Fat* Volume::getFat()
{
	return mFat;
}

int Volume::format()
{
	Mutex mutex(critical);
	while (mFileTable->numOfOpenFiles() > 0)
	{
		formatAnounce = true;
		SleepConditionVariableCS(&formatBlocked, &critical, INFINITE);
	}

	for (int i = 0; i < mFat->clusterCount - 1; i++)
		mFat->setFatValue(i, i + 1);	//Chaning fat clusters in fat table

	mFat->setFatValue(mFat->clusterCount - 1, 0);
	

	mFat->clusters[0]->entries[FREE_CLUSTERS_ENTRY] = mFat->clusterCount + 1;	//First cluster of chaning free clusters
	mFat->clusters[0]->entries[FAT_SIZE_ENTRY] = mFat->clusterCount;	//Size of Fat Table
	mFat->clusters[0]->entries[ROOT_DIR_ENTRY] = mFat->clusterCount;	//Root dir cluster
	mFat->clusters[0]->entries[ROOT_DIR_SIZE_ENTRY] = 0;	//Size of Root Dir

	mFat->setFatValue(mFat->clusterCount, 0);//Root dir cluster

	for (int i = mFat->clusterCount + 1; i < mFat->fatSize() - 1; i++)
	{
		mFat->setFatValue(i, i + 1);
	}

	mFat->setFatValue(mFat->fatSize() - 1, 0);
	mFat->saveFat();

	formatAnounce = false;
	return 1;
}

void Volume::unmount()
{
	Mutex mutex(critical);
	while (mFileTable->numOfOpenFiles() > 0)
	{
		unmountAnounce = true;
		SleepConditionVariableCS(&unmountBlocked, &critical, INFINITE);
	}

	unmountAnounce = false;
}

KernelFile* Volume::open(char* fname, char mode)
{
	Mutex mutex(critical);
	if (formatAnounce == true || unmountAnounce == true) return nullptr;
	if (mode != 'w' && mode != 'r' && mode != 'a') return nullptr;
	if (fname[0] != mLetter || fname[1] != ':') return nullptr;
	if (mFileTable->isFileOpenByThread(fname, GetCurrentThreadId())) return nullptr;
	
	int exist = doesExists(fname);
	if (!exist)
	{
		if (mode == 'r' || mode == 'a')
			return nullptr;

		int ret = createFileSystemObject(fname, FILE_ATRIBUTE);
		if (!ret) return nullptr;
	}
	/*else
	{
		if (mode == 'w')
		{
			int ret = deleteFile(fname);
			if (!ret) return nullptr;
			ret = createFileSystemObject(fname, FILE_ATRIBUTE);
			if (!ret) return nullptr;
		}
	}*/

	if (!mFileTable->openFile(fname, critical)) return 0;

	vector<string> tokenizer = split(fname, '\\');
	if (!tokenizer[tokenizer.size() - 1].compare("")) tokenizer.erase(tokenizer.end() - 1);

	Cluster parrent(mPartition, mFat->getRootEntry());// = new Cluster(mPartition, mFat->getRootEntry());
	int ret = find(&parrent, mFat->getRootSize(), mFat->getRootEntry(), tokenizer.begin() + 1, tokenizer.end());
	if (ret == -1) return 0;

	Entry e;
	parrent.readEntry(e, ret);

	KernelFile *file = new KernelFile(fname, mode, &e, this);

	return file;
}

int Volume::close(string fname, EntryNum firstCluster, EntryNum size)
{
	Mutex mutex(critical);


	Partition *p = mPartition;
	Fat *f = mFat;
	Cluster *dir = new Cluster(p, f->getRootEntry());

	string splitFile = fname;
	splitFile.append(1, '\0');
	vector<string> tokenizer = split(&splitFile[0], '\\');
	int n = find(dir, f->getRootSize(), f->getRootEntry(), tokenizer.begin() + 1, tokenizer.end());

	Entry e;
	dir->readEntry(e, n);
	e.firstCluster = firstCluster;
	e.size = size;
	dir->writeEntry(e, n);

	delete dir;


	mFileTable->closeFile(fname, critical);
	if (mFileTable->numOfOpenFiles() == 0)
	{
		WakeAllConditionVariable(&formatBlocked);
		WakeAllConditionVariable(&unmountBlocked);
	}
	return 1;
}

int Volume::deleteFile(char *fname)
{
	Mutex mutex(critical);

	if (mFileTable->isOpen(fname)) return 0;

	return deleteFileSystemObject(fname, FILE_ATRIBUTE);
}

int Volume::doesExists(char *fname)
{
	Mutex mutex(critical);
	vector<string> tokenizer = split(fname, '\\');
	if (!tokenizer[tokenizer.size() - 1].compare("")) tokenizer.erase(tokenizer.end() - 1);
	if (tokenizer.size() == 0) return 0;

	string check;
	check.append(1, mLetter).append(1, ':');
	if ((tokenizer[0]).compare(check)) return 0;//It is not that root

	Cluster* parrent = new Cluster(mPartition, mFat->getRootEntry());
	int ret = find(parrent, mFat->getRootSize(), mFat->getRootEntry(), tokenizer.begin()+1, tokenizer.end());
	delete parrent;

	if (ret == -1) return 0;

	return 1;
}


int Volume::createDir(char *dirname)
{
	Mutex mutex(critical);
	return createFileSystemObject(dirname, DIR_ATRIBUTE);
}

int Volume::deleteDir(char *fname)
{
	Mutex mutex(critical);
	return deleteFileSystemObject(fname, DIR_ATRIBUTE);
}

char Volume::readDir(char* dirname, EntryNum n, Entry &e)
{
	Mutex mutex(critical);

	vector<string> tokenizer = split(dirname, '\\');
	if (!tokenizer[tokenizer.size() - 1].compare("")) tokenizer.erase(tokenizer.end() - 1);
	if (tokenizer.size() == 0) return 0; //Can't delete root directroy!

	string check;
	check.append(1, mLetter).append(1, ':');
	if ((tokenizer[0]).compare(check)) return 0;

	Cluster search(mPartition, mFat->getRootEntry());
	//bool searchRoot = false;
	EntryNum dirSize = -1;
	int firstDirCluster = -1;
	if (tokenizer.size() == 1)
	{
		//searchRoot = true;
		firstDirCluster = mFat->getRootEntry();
		dirSize = mFat->getRootSize();
	}
	else
	{
		int ret = find(&search, mFat->getRootSize(), mFat->getRootEntry(), tokenizer.begin() + 1, tokenizer.end());
		if (ret == -1) return 0;

		Entry desc;
		search.readEntry(desc,ret);
		dirSize = desc.size;
		firstDirCluster = desc.firstCluster;
	}

	if (dirSize <= n) return 2;
	
	EntryNum p = firstDirCluster;
	while (n >= MAX_DIR_CLUSTER_ENTRY)
	{
		p = mFat->getFatValue(p);
		if (p == 0) return 0;
		n -= MAX_DIR_CLUSTER_ENTRY;
	}

	search.setClusterNo(p);
	
	search.readEntry(e, n);
	return 1;
}

int Volume::find(Cluster *dirCluster, int dirSize, ClusterNo currDirCluster, vector<string>::iterator start, vector<string>::iterator end)
{
	Mutex mutex(critical);
	Cluster *c = dirCluster;
	if (dirSize == 0 || currDirCluster == 0) return -1;

	for (vector<string>::iterator it = start; it != end; it++)
	{
		string searchObject = *(it);
		string ext;
		if (!makeNameAndExt(searchObject, searchObject, ext)) return 0;


			bool levelFound = false;
			for (int i = 0; i < dirSize; i++)
			{
				if (i % MAX_DIR_CLUSTER_ENTRY == 0 && i != 0){
					currDirCluster = mFat->getFatValue(currDirCluster);

					if (currDirCluster == 0) return 0;

					c->setClusterNo(currDirCluster);
				}

				Entry desc;
				//Entry *desc = c->readDirEntry(i%MAX_DIR_CLUSTER_ENTRY);
				c->readEntry(desc, i%MAX_DIR_CLUSTER_ENTRY);
				if (memcmp(searchObject.c_str(), desc.name, FNAMELEN) == 0
					&& memcmp(ext.c_str(), desc.ext, FEXTLEN) == 0)
				{
					if (it == end - 1)//Token And Name EQUALS
					{
						return i%MAX_DIR_CLUSTER_ENTRY;	//If it is last in path seqvence it was founded
					}
					else if (desc.attributes != 2) return -1;	//else if it is not directory return 0
					else//else, get deeper in path seqvence
					{
						currDirCluster = desc.firstCluster;
						dirSize = desc.size;

						c->setClusterNo(currDirCluster);
						levelFound = true;
						break;
					}
				}
			}
			if (!levelFound) return -1;
		}

	return -1;
}

int Volume::createFileSystemObject(char *fsoname, int atribute)
{
	Mutex mutex(critical);

	Cluster *parrent = 0, *sub = 0;
	while (1)
	{
		vector<string> tokenizer = split(fsoname, '\\');
		if (!tokenizer[tokenizer.size() - 1].compare("")) tokenizer.erase(tokenizer.end() - 1);
		if (tokenizer.size() == 0 || tokenizer.size() == 1) break; //Can't create root directroy!

		string check;
		check.append(1, mLetter).append(1, ':');
		if ((tokenizer[0]).compare(check)) break;//It is not that root

		bool addToRoot = false;
		//Cluster *parrent = 0;
		int parrentRet = -1;
		if (tokenizer.size() == 2)
		{
			addToRoot = true;
			parrentRet = 0;
		}
		else
		{
			parrent = new Cluster(mPartition, mFat->getRootEntry());
			parrentRet = find(parrent, mFat->getRootSize(), mFat->getRootEntry(), tokenizer.begin() + 1, tokenizer.end() - 1);
			if (parrentRet == -1) break; //there is no that parrent directory
		}


		//Cluster *sub = 0;
		int dirSize = -1;
		int cluNo = -1;
		if (parrent == 0)
		{
			sub = new Cluster(mPartition, mFat->getRootEntry());
			dirSize = mFat->getRootSize();
			cluNo = mFat->getRootEntry();
		}
		else
		{
			Entry dsc;
			parrent->readEntry(dsc, parrentRet);
			sub = new Cluster(mPartition, dsc.firstCluster);
			dirSize = dsc.size;
			cluNo = dsc.firstCluster;
		}

		int subRet = find(sub, dirSize, cluNo, tokenizer.end() - 1, tokenizer.end());
		if (subRet != -1) break; //Last object in searchPath found, already exist
		//if (found == false && addToRoot == false) return 0;


		//if (found || save == -1)
		Entry bDD;// = 0;
		if (addToRoot)
		{
			bDD.attributes = 2;
			bDD.firstCluster = mFat->getRootEntry();
			bDD.size = mFat->getRootSize();
		}
		else parrent->readEntry(bDD, parrentRet); // directory + (save%MAX_DIR_CLUSTER_ENTRY);




		ClusterNo p = bDD.firstCluster, q = 0;
		while (p != 0)
		{
			q = p;
			p = mFat->getFatValue(p);
		}



		if (/*bDD->size > 0 && */(bDD.size % MAX_DIR_CLUSTER_ENTRY == 0))	//Dir cluster full, alocate new one
		{
			if (!(addToRoot && bDD.size == 0))
			{
				ClusterNo newCluster = mFat->allocateCluster();
				if (newCluster <= 0) break; //there is no space for shrinking directory


				if (q == 0)
				{
					bDD.firstCluster = newCluster;
					mFat->setFatValue(newCluster, 0);
				}
				else
				{
					mFat->setFatValue(q, newCluster);
				}

				q = newCluster;	//Now q is new Cluster
			}
		}

		sub->setClusterNo(q);//Cluster *cq = new Cluster(mPartition, q);

		Entry newDirDesc;
		string newFSOName = *(tokenizer.end() - 1);
		string newFSOExt;
		if (!makeNameAndExt(newFSOName, newFSOName, newFSOExt)) break;

		newDirDesc.attributes = atribute;
		std::memcpy(newDirDesc.name, newFSOName.c_str(), FNAMELEN * sizeof(char));
		std::memcpy(newDirDesc.ext, newFSOExt.c_str(), FEXTLEN * sizeof(char));
		newDirDesc.size = 0;
		newDirDesc.firstCluster = 0;//newDirCluster;
		sub->writeEntry(newDirDesc, bDD.size % MAX_DIR_CLUSTER_ENTRY);
		//
		bDD.size++;
		if (addToRoot)
		{
			mFat->setRootDirSize(bDD.size);
			mFat->saveFat();
		}
		else
		{
			parrent->writeEntry(bDD, parrentRet);
		}

		delete parrent;
		delete sub;

		return 1;
	}
	
	delete parrent;
	delete sub;
	return 0;
	
}

int Volume::deleteFileSystemObject(char *fsoname, int atribute)
{
	Mutex mutex(critical);

	Cluster *parrent = 0, *sub = 0;
	while (1)
	{
		vector<string> tokenizer = split(fsoname, '\\');
		if (!tokenizer[tokenizer.size() - 1].compare("")) tokenizer.erase(tokenizer.end() - 1);
		if (tokenizer.size() == 0 || tokenizer.size() == 1) break; //Can't delete root directroy!

		string check;
		check.append(1, mLetter).append(1, ':');
		if ((tokenizer[0]).compare(check)) break;

		bool deleteFromRoot = false;
		int parrentRet = -1;
		if (tokenizer.size() == 2)
		{
			deleteFromRoot = true;
			parrentRet = 0;
		}
		else
		{
			parrent = new Cluster(mPartition, mFat->getRootEntry());
			parrentRet = find(parrent, mFat->getRootSize(), mFat->getRootEntry(), tokenizer.begin() + 1, tokenizer.end() - 1);
			if (parrentRet == -1) break;
		}


		
		int dirSize = -1;
		int cluNo = -1;
		if (parrent == 0)
		{
			sub = new Cluster(mPartition, mFat->getRootEntry());
			dirSize = mFat->getRootSize();
			cluNo = mFat->getRootEntry();
		}
		else
		{
			Entry dsc;
			parrent->readEntry(dsc, parrentRet);
			sub = new Cluster(mPartition, dsc.firstCluster);
			dirSize = dsc.size;
			cluNo = dsc.firstCluster;
		}

		int subRet = find(sub, dirSize, cluNo, tokenizer.end() - 1, tokenizer.end());
		if (subRet == -1) break; //Last object in searchPath not found, so can't delete him

		Entry subDescHit;
		sub->readEntry(subDescHit, subRet);//if directory isn't empty

		if (subDescHit.attributes != atribute) break;
		if (subDescHit.size != 0 && atribute == DIR_ATRIBUTE) break;
		if (subDescHit.size > 0 && atribute == FILE_ATRIBUTE)//Dealocate file's cluster
		{
			ClusterNo p = subDescHit.firstCluster;
			ClusterNo q = p;
			while (p != 0)
			{
				p = mFat->getFatValue(p);
				mFat->setFatValue(q, 0);
				mFat->deallocateCluster(q);
				q = p;
			}
		}

		Entry bDD;


		if (deleteFromRoot == true)
		{
			bDD.attributes = 2;
			bDD.firstCluster = mFat->getRootEntry();
			bDD.size = mFat->getRootSize();
		}
		else
		{

			parrent->readEntry(bDD, parrentRet);
		}

		ClusterNo p = bDD.firstCluster, q = 0, r = 0;
		while (p != 0)
		{
			r = q;
			q = p;
			p = mFat->getFatValue(p);
		}

		Cluster *qCluster = 0;	
		qCluster = new Cluster(mPartition, q);
		Entry qDesc;
		qCluster->readEntry(qDesc, (dirSize - 1) % MAX_DIR_CLUSTER_ENTRY);


		sub->writeEntry(qDesc, subRet);
		delete qCluster;




		if (bDD.size % MAX_DIR_CLUSTER_ENTRY == 1)	//dealocating directory cluster
		{
			if (r != 0) mFat->setFatValue(r, 0);

			if (q != mFat->getRootEntry())	//Dont dealocate root first cluster
			{
				mFat->deallocateCluster(q);
				if (bDD.size == 1) bDD.firstCluster = 0;
			}
		}

		bDD.size--;
		if (deleteFromRoot == true)
		{
			mFat->setRootDirSize(bDD.size);
			mFat->saveFat();
		}
		else
		{
			parrent->writeEntry(bDD, parrentRet);
		}

		delete parrent;
		delete sub;

		return 1;
	}

	delete parrent;
	delete sub;
	return 0;

}

vector<string> Volume::split(const char *str, char c)
{
	vector<string> result;

	do
	{
		const char *begin = str;

		while (*str != c && *str)
			str++;

		result.push_back(string(begin, str));
	} while (0 != *(str++));

	return result;
}

int Volume::makeNameAndExt(string &fname, string &makeFname, string &makeExt)
{
	size_t found = fname.find_last_of(".");
	if (found == std::string::npos)
	{
		makeExt.append(FEXTLEN, ' ');
		makeFname = fname;
	}
	else
	{
		makeExt = fname.substr(found + 1, string::npos);
		if (makeExt.size() > FEXTLEN) return 0;
		makeExt.append(FEXTLEN - makeExt.length(), ' ');
		makeFname = fname.substr(0, found);
	}

	if (makeFname.size() > FNAMELEN) return 0;
	makeFname.append(FNAMELEN - makeFname.length(), ' ');
	return 1;
}
