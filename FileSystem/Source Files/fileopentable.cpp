#include "fileopentable.h"
#include "fileopenmonitor.h"
#include "mutex.h"


FileOpenTable::FileOpenTable()
{
	table = new unordered_map<string, TableEntry*>();
}

FileOpenTable::~FileOpenTable()
{
	table->clear();
	delete table;
}

TableEntry::TableEntry(string fn, string fl)
{
	fname = fn;
	flocation = fl;
	declared = 0;
	fm = new FileOpenMonitor();
	threads = new unordered_set<DWORD>();
}

TableEntry::~TableEntry()
{
	delete fm;
	threads->clear();
	delete threads;
}

int FileOpenTable::openFile(string fullfname, CRITICAL_SECTION& critical)
{
	unordered_map<string, TableEntry*>::const_iterator found = table->find(fullfname);
	TableEntry* entry = 0;
	if (found == table->end())
	{
		string fname = fullfname.substr(fullfname.find_last_of("\\") + 1, string::npos);
		string flocation = fullfname.substr(0, fullfname.find_last_of("\\"));
		entry = new TableEntry(fname, flocation);
		std::pair<string, TableEntry*> entry_pair(fullfname, entry);
		table->insert(entry_pair);// entry_pair);
	}
	else
	{
		entry = found->second;
		int threadId = GetCurrentThreadId();
		bool is_in_thread_ID = (entry->threads)->find(threadId) != (entry->threads)->end();
		if (is_in_thread_ID == true) return 0;
	}

	int threadId = GetCurrentThreadId();
	(entry->threads)->insert(threadId);
	entry->declared++;
	(entry->fm)->openFile(critical);
	
	return 1;
}

int FileOpenTable::closeFile(string fullfname, CRITICAL_SECTION& critical)
{
	unordered_map<string, TableEntry*>::const_iterator found = table->find(fullfname);
	
	if (found == table->end()) return 0;

	TableEntry* entry = found->second;
	int threadId = GetCurrentThreadId();

	unordered_set<DWORD>::const_iterator idFound = (entry->threads)->find(threadId);

	if (idFound == (entry->threads)->end()) return 0;

	(entry->threads)->erase(idFound);
	entry->declared--;

	if (entry->declared == 0) table->erase(found);

	(entry->fm)->closeFile(critical);

	return 1;
}

int FileOpenTable::isFileOpenByThread(string fullfname, DWORD id)
{
	unordered_map<string, TableEntry*>::const_iterator found = table->find(fullfname);

	if (found == table->end()) return 0;

	TableEntry* entry = found->second;

	unordered_set<DWORD>::const_iterator idFound = (entry->threads)->find(id);

	if (idFound == (entry->threads)->end()) return 0;
	else return 1;
}

int FileOpenTable::isOpen(string fullfname)
{
	unordered_map<string, TableEntry*>::const_iterator found = table->find(fullfname);

	if (found == table->end()) return 0;

	else return 1;
}

int FileOpenTable::numOfOpenFiles()
{
	return table->size();
}