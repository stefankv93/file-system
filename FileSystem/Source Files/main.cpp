#include "testprimer.h"

using namespace std;

HANDLE nit1,nit2,nit3;
DWORD ThreadID;

HANDLE semMain=CreateSemaphore(NULL,0,32,NULL);
HANDLE sem12=CreateSemaphore(NULL,0,32,NULL);
HANDLE sem13=CreateSemaphore(NULL,0,32,NULL);
HANDLE sem21=CreateSemaphore(NULL,0,32,NULL);
HANDLE sem23=CreateSemaphore(NULL,0,32,NULL);
HANDLE sem31=CreateSemaphore(NULL,0,32,NULL);
HANDLE sem32=CreateSemaphore(NULL,0,32,NULL);
HANDLE mutex=CreateSemaphore(NULL,1,32,NULL);

Partition *partition1, *partition2;
char p1,p2;

char *ulazBuffer;
int ulazSize;

ostream& operator<<(ostream &os,const Entry &E){
	char name[13];
	memcpy(name,E.name,8);
	name[8]='.';
	memcpy(name+9,E.ext,3);
	name[12]=0;
	return os<<name<<" ["<<E.size<<']';
}
//

int main()
{
	
	/*{//ucitavamo ulazni fajl u bafer, da bi nit 1 i 2 mogle paralelno da citaju
		FILE *f = fopen("ulaz.dat", "rb");
		if (f == 0){
			cout << "GRESKA: Nije nadjen ulazni fajl 'ulaz.dat' u os domacinu!" << endl;
			system("PAUSE");
			return 0;//exit program
		}
		ulazBuffer = new char[32 * 1024 * 1024];//32MB
		ulazSize = fread(ulazBuffer, 1, 32 * 1024 * 1024, f);
		fclose(f);
	}

	partition1 = new Partition("p1.ini");
	p1 = FS::mount(partition1);
	FS::format(p1);

	{
		for (int i = 0; i < 250; i++)
		{
			char rootpath[] = "1:\\";
			rootpath[0] = p1;
			string s = rootpath;
			s.append("Stex", 4);
			char *num = new char[5];
			itoa(i, num, 10);
			s.append(num, strlen(num));
			FS::createDir(&s[0]);
		}

		FS::deleteDir("A:\\Stex5");
		FS::deleteDir("A:\\Stex18");
	}

	{
		char filepath[] = "1:\\Stex25\\fajl1.dat";
		filepath[0] = p1;
		for (int i = 0; i < 5; i++)
		{
			File *f = 0;
			if (i == 0)
				f = FS::open(filepath, 'w');
			else f = FS::open(filepath, 'a');

			int cursor = f->getFileSize();
			int toCopy = ulazSize / 5;
			f->write(toCopy, ulazBuffer + cursor);
			f->seek(0);
			delete f;
		}
	}

	
	{
		Entry D[280];
		p1 = FS::mount(partition1);
		char rootpath[] = "1:\\Stex25";
		rootpath[0] = p1;
		int lim = 0, fin = 0;
		while (!fin){
			if (FS::readDir(rootpath, lim, D[lim]) == 2)
				fin = 1;
			else
			{
				lim++;
			}
				
		}

		cout << '\t' << p1 << ':' << endl;
		for (int i = 0; i < lim; i++) cout << "\t" << ((i != lim - 1) ? char(195) : char(192)) << ' ' << D[i] << endl;
	}



	{
		char filepath[] = "1:\\Stex25\\fajl1.dat";
		filepath[0] = p1;
		File *f = FS::open(filepath, 'a');
		//f->seek(2047);
		//f->truncate();
		//char *dodatak = "JEBOTE!!";
		//f->write(8, dodatak);
		//wait(mutex); cout << "Nit1: Otvoren fajl 'fajl1.dat'" << endl; signal(mutex);
		ofstream fout("izlaz1.dat", ios::out | ios::binary);
		char *buff = new char[f->getFileSize()];
		f->seek(0);
		for (int i = 0; i < 5; i++)
		{
			int toRead = f->getFileSize() / 5;
			f->read(toRead, buff);
			fout.write(buff, toRead);
		}
		delete[] buff;
		fout.close();
		delete f;
		//FS::unmount(p1);
	}

	FS::deleteDir("A:\\Stex25");
	FS::deleteFile("A:\\Stex25\\fajl1.dat");
	

	{
		Entry D[280];
		p1 = FS::mount(partition1);
		char rootpath[] = "1:\\Stex25";
		rootpath[0] = p1;
		int lim = 0, fin = 0;
		while (!fin){
			if (FS::readDir(rootpath, lim, D[lim]) == 2)
				fin = 1;
			else
			{
				lim++;
			}

		}

		cout << '\t' << p1 << ':' << endl;
		for (int i = 0; i < lim; i++) cout << "\t" << ((i != lim - 1) ? char(195) : char(192)) << ' ' << D[i] << endl;
		}*/
	clock_t startTime,endTime;
	cout<<"Pocetak testa!"<<endl;
	startTime=clock();//pocni merenje vremena
	{//ucitavamo ulazni fajl u bafer, da bi nit 1 i 2 mogle paralelno da citaju
		FILE *f=fopen("ulaz.dat","rb");
		if(f==0){
			cout<<"GRESKA: Nije nadjen ulazni fajl 'ulaz.dat' u os domacinu!"<<endl;
			system("PAUSE");
			return 0;//exit program
		}
		ulazBuffer=new char[32*1024*1024];//32MB
		ulazSize=fread(ulazBuffer, 1, 32*1024*1024, f);
		fclose(f);
	}

	nit1=CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE) nit1run,NULL,0,&ThreadID); //kreira i startuje niti
	nit2=CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE) nit2run,NULL,0,&ThreadID);
	nit3=CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE) nit3run,NULL,0,&ThreadID);

	for(int i=0; i<3; i++) wait(semMain);//cekamo da se niti zavrse
	delete [] ulazBuffer;
	endTime=clock();
	cout<<"Kraj test primera!"<<endl;
	cout<<"Vreme izvrsavanja: "<<((double)(endTime-startTime)/((double)CLOCKS_PER_SEC/1000.0))<<"ms!"<<endl;
	CloseHandle(mutex);
	CloseHandle(semMain);
	CloseHandle(sem12);
	CloseHandle(sem13);
	CloseHandle(sem21);
	CloseHandle(sem23);
	CloseHandle(sem31);
	CloseHandle(sem32);
	CloseHandle(nit1);
	CloseHandle(nit2);
	CloseHandle(nit3);

	//cin.get();
	return 0;
}