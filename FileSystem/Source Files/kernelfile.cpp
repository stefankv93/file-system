#include "kernelfile.h"
#include "volume.h"
#include "cluster.h"
#include "fat.h"

KernelFile::KernelFile(char *fname, char mode, Entry *e, Volume *v)
{
	this->fname = fname;
	this->mode = mode;
	mVolume = v;
	size = e->size;
	firstCluster = e->firstCluster;
	p = firstCluster;
	q = 0;
	data = new Cluster(mVolume->getPartition(), firstCluster);

	switch (mode)
	{
	case 'r': seek(0); break;
	case 'w': seek(0); truncate(); break;
	case 'a': seek(size);
	}

	/*if (mode == 'w' || mode == 'r') 
		pos = 0;
	else seek(size);*/


}

KernelFile::~KernelFile()
{
	//EnterCriticalSection(&mVolume->critical);
	
	
	mVolume->getFat()->saveFat();
	//LeaveCriticalSection(&mVolume->critical);
	mVolume->close(fname, firstCluster, size);
	delete data;
}

char KernelFile::write(BytesCnt cnt, char* buffer)
{
	if (mode == 'r') return 0;

	int i = 0;
	int copied = 0;
	while (cnt>0)
	{
		
			if (p == 0)	//there is no more space, allocate new cluster for file
			{
				p = mVolume->getFat()->allocateCluster();
				if (p == 0) return 0; //there is no more space;

				if (firstCluster == 0 && size == 0)//alocate new first cluster
				{
					firstCluster = p;
					//mVolume->getFat()->setFatValue(firstCluster, 0);
				}
				else mVolume->getFat()->setFatValue(q, p);//alocate new cluster
				//int pp = mVolume->getFat()->getFatValue(q);
				//dirty = true;
			}
			//q = p;
			//p = p = mVolume->getFat()->getFatValue(p);


		BytesCnt clusterPos = pos%MAX_CLUSTER_BYTES;
		BytesCnt toCopy = -1;
		if (MAX_CLUSTER_BYTES - clusterPos > cnt)
			toCopy = cnt;
		else toCopy = MAX_CLUSTER_BYTES - clusterPos;
		data->setClusterNo(p);
		data->writeBytes(copied + buffer, toCopy, clusterPos);
		//data->save();
		pos += toCopy;
		cnt -= toCopy;
		copied += toCopy;
		if (pos > size) size = pos;

		if (pos%MAX_CLUSTER_BYTES == 0)
		{
			q = p;
			p = mVolume->getFat()->getFatValue(p);
		}
		i++;
	}
	return 1;
}

BytesCnt KernelFile::read(BytesCnt cnt, char* buffer)
{
	int i = 0;
	int read = 0;
	while (cnt > 0 && pos<size)
	{
		BytesCnt clusterPos = pos%MAX_CLUSTER_BYTES;

		BytesCnt toEnd = size - pos;
		BytesCnt toCopy = -1;
		if (MAX_CLUSTER_BYTES - clusterPos > cnt)
			toCopy = cnt;
		else toCopy = MAX_CLUSTER_BYTES - clusterPos;

		if (toCopy > toEnd) toCopy = toEnd;

		data->setClusterNo(p);
		data->readBytes(read + buffer, toCopy, clusterPos);
		pos += toCopy;
		cnt -= toCopy;
		read += toCopy;

		if (pos%MAX_CLUSTER_BYTES == 0)
		{
			q = p;
			p = mVolume->getFat()->getFatValue(p);
		}
		i++;
	}
	return read;
}
char KernelFile::seek(BytesCnt newPos)
{
	if (newPos > size) return 0;

	pos = newPos;

	int numOfFileCluster = pos / 2048;

	Fat* fat = mVolume->getFat();
	p = firstCluster; q = 0;
	while (numOfFileCluster > 0 && p!=0)
	{
		q = p;
		p = fat->getFatValue(p);
		numOfFileCluster--;
	}

	data->setClusterNo(p);
	return 1;
}

BytesCnt KernelFile::filePos()
{
	return pos;
}
char KernelFile::eof()
{
	if (pos < size) return 0;
	else if (pos == size) return 2;
	else return 1;
}
BytesCnt KernelFile::getFileSize()
{
	return size;
}
char KernelFile::truncate()
{
	if (pos < size && firstCluster!=0)
	{
		//if (firstCluster != 0)
		{
			size = pos;
			ClusterNo c;
			if (pos%MAX_CLUSTER_BYTES == 0)
			{
				c = p;
				p = 0;
				if (q == 0) firstCluster = 0;
				else mVolume->getFat()->setFatValue(q, 0);
			}
			else
			{
				c = mVolume->getFat()->getFatValue(p);
				mVolume->getFat()->setFatValue(p, 0);
			}
			ClusterNo cq = c;
			//ClusterNo c = mVolume->getFat()->getFatValue(p), cq = c;
			
			while (c != 0)
			{
				c = mVolume->getFat()->getFatValue(c);
				mVolume->getFat()->setFatValue(cq, 0);
				mVolume->getFat()->deallocateCluster(cq);
				cq = c;
			}

			return 1;
		}

		//return 0;
	}
	else return 0;
}