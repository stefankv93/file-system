#include "thread.h"
#include <process.h>

Thread::Thread()
{
	handler = (HANDLE)_beginthreadex(0, 0, ThreadEntry, this, CREATE_SUSPENDED, &threadId);
}

Thread::~Thread()
{
	CloseHandle(handler);
}

unsigned int Thread::ThreadEntry(void *arg)
{
	Thread* activeObject = (Thread*)arg;
	activeObject->run();
	return 0;
}

void Thread::start()
{
	ResumeThread(handler);
}

void Thread::join()
{
	WaitForSingleObject(handler, INFINITE);
}