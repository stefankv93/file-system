
#include "part.h"
#include <iostream>
#include "thread.h"
#include "kernelfs.h"
#include "bounded.h"
#include "fat.h"
#include <string>
#include <sstream>
#include "volume.h"
#include "file.h"
using namespace std;

/*struct Entry {
	char name[8];
	char ext[3];
	char attributes;
	unsigned long firstCluster;
	unsigned long size;
};*/


int main()
{
	KernelFS *kf = new KernelFS();
	Partition *p1 = new Partition("p1.ini");
	kf->mount(p1);
	//kf->format('A');
	/*for (int i = 0; i < 105; i++)
	{
		string c = "A:\\Stex";

		string Result;          // string which will contain the result

		ostringstream convert;   // stream used for the conversion

		convert << i;      // insert the textual representation of 'Number' in the characters in the stream

		Result = convert.str();

		c.append(Result);
		c.append(1, '\\');
		c.append(1, 0);

		int ret = kf->createDir(&c[0]);
		cout << i << ". " << ret << endl;
	}

	int novi = kf->createDir("A:\\Stex0\\XXX");
	cout << "Novi XXX: " << novi << endl;
	novi = kf->createDir("A:\\Stex0\\XXX\\JEBIGA");
	cout << "Novi JEBIGA: " << novi << endl;
	novi = kf->deleteDir("A:\\Stex0\\XXX\\JEBIGA");
	cout << "DELETE JEBIGA: " << novi << endl;
	novi = kf->doesExist("A:\\Stex0\\XXX\\JEBIGA");
	cout << "EXISTS JEBIGA XXX:" << novi << endl;

	Entry e;
	novi = kf->readDir("A:\\", 101, e);
	if (novi == 1)
	{
		cout << "Dir name: " << e.name << "Dir size: " << e.size <<endl;
	}
	else if (novi == 2) cout << "NEUSPELO CITANJE DIRA, PREKORACENJE: " << novi << endl;
	else cout << "NEUSPELO CITANJE DIRA: " << novi << endl;

	novi = kf->readDir("A:\\", 102, e);
	if (novi == 1)
	{
		cout << "Dir name: " << e.name << "Dir size: " << e.size << endl;
	}
	else if (novi == 2) cout << "NEUSPELO CITANJE DIRA, PREKORACENJE: " << novi << endl;
	else cout << "NEUSPELO CITANJE DIRA: " << novi << endl;

	novi = kf->readDir("A:\\Taraba", 110, e);
	if (novi == 1)
	{
		cout << "Dir name: " << e.name << "Dir size: " << e.size << endl;
	}
	else if(novi == 2) cout << "NEUSPELO CITANJE DIRA, PREKORACENJE: " << novi << endl;
	else cout << "NEUSPELO CITANJE DIRA: " << novi << endl;

	novi = kf->readDir("A:\\Stex0", 110, e);
	if (novi == 1)
	{
		cout << "Dir name: " << e.name << "Dir size: " << e.size << endl;
	}
	else if (novi == 2) cout << "NEUSPELO CITANJE DIRA, PREKORACENJE: " << novi << endl;
	else cout << "NEUSPELO CITANJE DIRA: " << novi << endl;

	for (int i = 0; i < 105; i++)
	{
		string c = "A:\\Stex";

		string Result;          // string which will contain the result

		ostringstream convert;   // stream used for the conversion

		convert << i;      // insert the textual representation of 'Number' in the characters in the stream

		Result = convert.str();

		c.append(Result);
		c.append(1, '\\');
		c.append(1, 0);

		int ret = kf->doesExist(&c[0]);
		cout <<i<<". " << ret <<endl;
	}

	cout << "Brisanje!" << endl;

	for (int i = 0; i < 105; i++)
	{
		string c = "A:\\Stex";

		string Result;          // string which will contain the result

		ostringstream convert;   // stream used for the conversion

		convert << i;      // insert the textual representation of 'Number' in the characters in the stream

		Result = convert.str();

		c.append(Result);
		c.append(1, '\\');
		c.append(1, 0);

		int ret = kf->deleteDir(&c[0]);
		cout << i << ". " << ret << endl;
	}

	//cout << "Potvrda brisanja!" << endl;

	/*for (int i = 0; i < 105; i++)
	{
		string c = "A:\\Stex";

		string Result;          // string which will contain the result

		ostringstream convert;   // stream used for the conversion

		convert << i;      // insert the textual representation of 'Number' in the characters in the stream

		Result = convert.str();

		c.append(Result);
		c.append(1, '\\');
		c.append(1, 0);

		int ret = kf->doesExist(&c[0]);
		cout << i << ". " << ret << endl;
	}*/
	kf->format('A');

	/*unsigned long *fatcluster = new unsigned long[512];
	p1->readCluster(0, (char *)fatcluster);

	cout << "Fat prvih 20:" << endl;
	for (int i = 0; i < 20; i++)
	{
		cout << fatcluster[i] << endl;
	}
	//int ret = kf->doesExist("A:\\Stex\\");
	//cout << ret;*/
	
	char *buffer = new char[4096];
	for (int i = 0; i < 4096; i++)
		buffer[i] = i%128;

	File *f = kf->open("A:\\Stex.txt", 'w');
	f->write(3000, (char*)buffer);
	delete f;

	f = kf->open("A:\\Stex.txt", 'r');
	int ret = f->read(3001, buffer);
	delete f;
	cout << ret << endl;

	for (int i = 10; i < 30; i++)
	{
		char c = buffer[i];
		cout << (int)c << endl;
	}

	/*char *firstCluster = new char[2048];
	p1->readCluster(5, (char *)firstCluster);

	cout << "3. klaster prvih 20:" << endl;
	for (int i = 0; i < 20; i++)
	{
		char c = firstCluster[i];
		cout << (int)c<< endl;
	}*/

	

	/*p1->readCluster(0, (char *)fatcluster);

	cout << "Fat prvih 20:" << endl;
	for (int i = 0; i < 20; i++)
	{
		cout << fatcluster[i] << endl;
	}*/
	//File *f1 = kf->open("A:\\Stex.txt", 'r');


	kf->unmount('A');
	delete p1;
	delete kf;
	
	/*KernelFS *kf = new KernelFS();

	int ret = kf->doesExist("X:\\Stex\\XXX\\Care");
	delete kf;*/

	/* Testing mount and unmount
	KernelFS *kf = new KernelFS();
	Partition *p1 = new Partition("p1.ini");
	cout << kf->mount(p1) << endl;
	cout << kf->mount(p1) << endl;
	cout << kf->mount(p1) << endl;
	cout << kf->unmount('B') << endl;
	cout << kf->unmount('B') << endl;
	cout << kf->mount(p1) << endl;
	cout << kf->unmount('C') << endl;
	cout << kf->unmount('B') << endl;
	cout << kf->unmount('A') << endl;*/

	/*unsigned long initFat[512];
	for (int i = 0; i < 512; i++)
	{
		initFat[i] = i;
	}

	p1->writeCluster(0, (char*)initFat);

	for (int i = 512; i < 2*512; i++)
	{
		initFat[i-512] = i;
	}
	p1->writeCluster(1, (char*)initFat);

	Fat *fat = new Fat(p1);

	cout << fat->getFatValue(506)<<endl;
	cout << fat->getFatValue(507) << endl;
	cout << fat->getFatValue(508) << endl;
	cout << fat->getFatValue(509) << endl;


	fat->setFatValue(15, 11111);
	fat->setFatValue(510, 22222);
	fat->setFatValue(511, 33333);
	fat->setFatValue(512, 44444);

	cout << fat->getFatValue(15) << endl;
	cout << fat->getFatValue(510) << endl;
	cout << fat->getFatValue(511) << endl;
	cout << fat->getFatValue(512) << endl;

	cout << sizeof(unsigned long) << endl;*/

	/*char c = kf->mount(p1);
	cout << c << endl;
	kf->format(c);

	unsigned long fat[512];
	p1->readCluster(1, (char*)fat);
	for (int i = 480; i < 500; i++)
		cout << i << ". " << fat[i] << endl;




	delete p1;
	delete kf;*/



	return 0;
}