#include "mounter.h"
#include "bounded.h"
#include "part.h"
#include <Windows.h>
#include <iostream>

Mounter::Mounter(BoundedBuffer<MounterRequest> *bb) : end(false), myBuffer(bb)
{
	for (int i = 0; i < PARTITION_NUM; i++)
		partitions[i] = nullptr;
	start();
}

Mounter::~Mounter()
{
	for (int i = 0; i < PARTITION_NUM; i++)
		delete partitions[i];
}


void Mounter::run()
{
	while (!end)
	{
		MounterRequest *mr = myBuffer->take();
		if (mr->mount)
		{
			int freeCount = 0;
			char letter = '0';
			for (; freeCount < PARTITION_NUM; freeCount++)
				if (partitions[freeCount] == nullptr)
					break;

			if (freeCount != PARTITION_NUM)
			{
				partitions[freeCount] = mr->partition;
				letter = 'A' + freeCount;
			}
			mr->letter = letter;
			ReleaseSemaphore(mr->semaphore, 1, 0);
		}
		else
		{

			int partitionIndex = mr->letter - 'A';
			if (partitionIndex >= 0 && partitionIndex < 26)
			{
				if (partitions[partitionIndex] != nullptr) {
					partitions[partitionIndex] = nullptr;
					ReleaseSemaphore(mr->semaphore, 1, 0);
				}
				else
				{
					mr->letter = '0';
					ReleaseSemaphore(mr->semaphore, 1, 0);
				}
			}
			else
			{
				mr->letter = '0';
				ReleaseSemaphore(mr->semaphore, 1, 0);
			}

		}
	}
}

void Mounter::stop()
{
	end = true;
}

Partition* Mounter::getPartition(char part)
{
	if ((part - 'A') >= 0 && (part - 'A') < PARTITION_NUM)
		return partitions[part - 'A'];
	else return nullptr;
}