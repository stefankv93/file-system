#include "mutex.h"


Mutex::Mutex(CRITICAL_SECTION& m) :mutex(m) {
	EnterCriticalSection(&m);
}

Mutex::~Mutex()
{
		LeaveCriticalSection(&mutex);
}